## Docker Nginx Template

- 安装docker

```
curl -sSL http://acs-public-mirror.oss-cn-hangzhou.aliyuncs.com/docker-engine/internet | sh -
```

- 安装docker compose （可以手动下载）

```
curl -L https://github.com/docker/compose/releases/download/1.12.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```
    
- 常用命令

```
#启动
docker-compose up -d

#停止
docker-compose down

#查看运行中的Docker
docker-compose ps

#查看LOG
docker-compose logs -f

#进入运行中的Docker
docker exec -i -t <docker name or id> bash

```